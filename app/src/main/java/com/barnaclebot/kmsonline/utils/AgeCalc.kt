package com.barnaclebot.kmsonline.utils

import android.annotation.SuppressLint
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object AgeCalculator {
    fun calculateAge(birthDate: Date): Age {
        var years = 0
        var months = 0
        var days = 0

        //create calendar object for birth day
        val birthDay: Calendar = Calendar.getInstance()
        birthDay.timeInMillis = birthDate.time

        //create calendar object for current day
        val currentTime = System.currentTimeMillis()
        val now: Calendar = Calendar.getInstance()
        now.timeInMillis = currentTime

        //Get difference between years
        years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR)
        val currMonth: Int = now.get(Calendar.MONTH) + 1
        val birthMonth: Int = birthDay.get(Calendar.MONTH) + 1

        //Get difference between months
        months = currMonth - birthMonth

        //if month difference is in negative then reduce years by one
        //and calculate the number of months.
        if (months < 0) {
            years--
            months = 12 - birthMonth + currMonth
            if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) months--
        } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            years--
            months = 11
        }

        //Calculate the days
        if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE)) days =
            now.get(Calendar.DATE) - birthDay.get(Calendar.DATE) else if (now.get(Calendar.DATE) < birthDay.get(
                Calendar.DATE
            )
        ) {
            val today: Int = now.get(Calendar.DAY_OF_MONTH)
            now.add(Calendar.MONTH, -1)
            days =
                now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today
        } else {
            days = 0
            if (months == 12) {
                years++
                months = 0
            }
        }
        //Create new Age object
        return Age(days, months, years)
    }

    fun calculateAgeInMonths(age: Age): Int {
        var ageMonth = 0
        if (age.years >= 1 && age.days >= 16) {
            ageMonth = (age.years*12)+age.months+1
        } else if (age.years >= 1 && age.days < 16) {
            ageMonth = (age.years*12)+age.months
        } else if (age.years < 1 && age.months >= 1 && age.days >= 16) {
            ageMonth = age.months+1
        } else if (age.years < 1 && age.months >= 1 && age.days < 16) {
            ageMonth = age.months
        } else if (age.years < 1 && age.months < 1 && age.days >= 16) {
            ageMonth = 1
        } else if (age.years < 1 && age.months < 1 && age.days < 16) {
            ageMonth = 0
        }
        return ageMonth
    }

    @Throws(ParseException::class)
    @SuppressLint("SimpleDateFormat")
    fun parseDate(date: String): Date {
        val sdf = SimpleDateFormat("dd/MM/yyyy")
        return sdf.parse(date)
    }

//    @Throws(ParseException::class)
//    @JvmStatic
//    fun main(args: Array<String>) {
//        val sdf = SimpleDateFormat("dd/MM/yyyy")
//        val birthDate: Date = sdf.parse("29/11/1981")
//        val age = calculateAge(birthDate)
//        println(age)
//    }
}