package com.barnaclebot.kmsonline.utils

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import org.joda.time.PeriodType
import java.io.BufferedReader
import java.io.IOException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.Period
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.abs

object ValueFormat {
    fun getJsonDataFromAsset(context: Context, fileName: String): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }

    fun getCsvDataFromAsset(context: Context, fileName: String): List<String>? {
        val data: List<String>
        try {
            data = context.assets.open(fileName).bufferedReader().use { it.readLines() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return data
    }

    @SuppressLint("SimpleDateFormat")
    fun convertLongToTime(time: Long): String {
        val date = Date(time)
        val format = SimpleDateFormat("dd/MM/yyyy")
        return format.format(date)
    }

    fun formatInUtcMilisecond(value: String): Long {
        val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
        return sdf.parse(value).time
    }

//    fun calculateMonth(days: Int): Boolean {
//        val month = days/30.4167
//        val finalAge = age + 16
//        val finalMonth = month * 3
//        return finalAge >= finalMonth
//    }

    fun formatDateNow(): String {
        val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
        return sdf.format(Date()).toString()
    }

    fun getYearBOD(dateBOD: String) : Int {
        val data = dateBOD.split("/")
        return data[2].toInt()
    }

    fun getMonthBOD(dateBOD: String) : Int {
        val data = dateBOD.split("/")
        return data[1].toInt()
    }

    fun getDaysBOD(dateBOD: String) : Int {
        val data = dateBOD.split("/")
        return data[0].toInt()
    }

    fun getAgeMonth(dateBOD: String) : Int{
        val birthdate = org.joda.time.LocalDate(
            getYearBOD(dateBOD), getMonthBOD(dateBOD), getDaysBOD(dateBOD)
        )
        val now = org.joda.time.LocalDate()
        return org.joda.time.Period(birthdate, now, PeriodType.yearMonthDay()).months
    }

    fun format2digit(value: String): Float {
        return String.format("%.2f", value).toFloat()
    }

    fun validateValue(value: String) : Boolean {
        return if (value.toFloat() < 100) {
            return if (value.length == 5) {
                value.contains(".")
            } else {
                true
            }
        } else {
            false
        }
    }

}