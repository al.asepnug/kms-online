package com.barnaclebot.kmsonline.di

import android.content.Context
import androidx.room.Room
import com.barnaclebot.kmsonline.data.local.db.AppDatabase
import com.barnaclebot.kmsonline.data.local.db.DatabaseDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): AppDatabase {
        return Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            "kms.db"
        ).build()
    }

    @Provides
    fun provideUserDao(appDatabase: AppDatabase): DatabaseDao {
        return appDatabase.databaseDao()
    }
}