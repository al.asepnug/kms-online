package com.barnaclebot.kmsonline.data

import androidx.lifecycle.LiveData
import com.barnaclebot.kmsonline.data.entity.*

interface DataSource {
    fun deleteAllData()

    fun getIsLogin(): Boolean

    fun setIsLogin(isLogin: Boolean)

    fun getKuesionerVersioning(): Int

    fun setKuesionerVersioning(value: Int)

    fun getPertumbuhanVersioning(): Int

    fun setPertumbuhanVersioning(value: Int)

    fun getUser() : LiveData<User>

    fun insertUser(user: User)

    fun updateUser(idUser: Int, user: User)

    fun getKuesioner(umur: Int) : LiveData<List<KuesionerPerkembangan>>

    fun insertKuesioner(kuesioner: List<KuesionerPerkembangan>)

    fun insertHasilKuesioner(data: HasilKuesionerPerkembangan)

    fun getHasilKuesionerByUsia(umur: Int) : LiveData<HasilKuesionerPerkembangan>

    fun getRecordBb() : LiveData<List<RecordBb>>

    fun getRecordBbByUsia(umur: Int) : LiveData<RecordBb>

    fun insertRecordBb(data: RecordBb)

    fun getRecordTb() : LiveData<List<RecordTb>>

    fun getRecordTbByUsia(umur: Int) : LiveData<RecordTb>

    fun insertRecordTb(data: RecordTb)

    fun getBeratBadan(gender: Int) : LiveData<List<BeratBadan>>

    fun getKesimpulanBeratBadan(gender: Int, umur: Int) : LiveData<BeratBadan>

    fun insertBeratBadan(beratBadan: BeratBadan)

    fun getTinggiBadan(gender: Int) : LiveData<List<TinggiBadan>>

    fun getKesimpulanTinggiBadan(gender: Int, umur: Int) : LiveData<TinggiBadan>

    fun insertTinggiBadan(tinggiBadan: TinggiBadan)
}