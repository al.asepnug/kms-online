package com.barnaclebot.kmsonline.data.entity

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "kuesioner_perkembangan")
data class KuesionerPerkembangan(

	@field:SerializedName("usia")
	val umur: Int? = null,

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("soal")
	val soal: String? = null,

	@field:SerializedName("rangeMax")
	val rangeMax: Int? = null,

	@field:SerializedName("rangeMin")
	val rangeMin: Int? = null,

	@PrimaryKey
	@NonNull
	@field:SerializedName("id")
	val id: Int? = null
)
