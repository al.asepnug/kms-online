package com.barnaclebot.kmsonline.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "record_bb")
data class RecordBb(
    @PrimaryKey
    val umur: Int,

    val beratBadan: Float,

    val tanggalRecord: String
)