package com.barnaclebot.kmsonline.data

import androidx.lifecycle.LiveData
import com.barnaclebot.kmsonline.data.entity.*
import com.barnaclebot.kmsonline.data.local.db.LocalDataSource
import com.barnaclebot.kmsonline.data.local.prefs.PrefsDataSource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class DataRepository @Inject constructor(
    private val prefsDataSource: PrefsDataSource,
    private val localDataSource: LocalDataSource
) : DataSource {

    override fun deleteAllData() {
        CoroutineScope(Dispatchers.IO).launch {
            prefsDataSource.removeAllData()
            localDataSource.deleteUser()
            localDataSource.deleteRecordBb()
            localDataSource.deleteRecordTb()
            localDataSource.deleteHasilKuesioner()
            localDataSource.deleteKuesioner()
            localDataSource.deleteTinggiBadan()
            localDataSource.deleteBeratBadan()
            prefsDataSource.setIsLogin(false)
        }
    }

    override fun getIsLogin(): Boolean = prefsDataSource.getIsLogin()

    override fun setIsLogin(isLogin: Boolean) {
        CoroutineScope(Dispatchers.IO).launch {
            prefsDataSource.setIsLogin(isLogin)
        }
    }

    override fun getKuesionerVersioning(): Int = prefsDataSource.getKuesionerVersioning()

    override fun setKuesionerVersioning(value: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            prefsDataSource.setKuesionerVersioning(value)
        }
    }

    override fun getPertumbuhanVersioning(): Int = prefsDataSource.getPertumbuhanVersioning()

    override fun setPertumbuhanVersioning(value: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            prefsDataSource.setPertumbuhanVersioning(value)
        }
    }

    override fun getUser(): LiveData<User> = localDataSource.getUser()

    override fun insertUser(user: User) {
        CoroutineScope(Dispatchers.IO).launch {
            localDataSource.deleteUser()
            localDataSource.insertUser(user)
        }
    }

    override fun updateUser(idUser: Int, user: User) {
        CoroutineScope(Dispatchers.IO).launch {
            localDataSource.updateUser(idUser, user)
        }
    }

    override fun getKuesioner(umur: Int): LiveData<List<KuesionerPerkembangan>> = localDataSource.getKuesioner(umur)

    override fun insertKuesioner(kuesioner: List<KuesionerPerkembangan>) {
        CoroutineScope(Dispatchers.IO).launch {
            localDataSource.deleteKuesioner()
            localDataSource.insertKuesioner(kuesioner)
        }
    }

    override fun insertHasilKuesioner(data: HasilKuesionerPerkembangan) {
        CoroutineScope(Dispatchers.IO).launch {
            localDataSource.insertHasilKuesioner(data)
        }
    }

    override fun getHasilKuesionerByUsia(umur: Int): LiveData<HasilKuesionerPerkembangan> = localDataSource.getKesimpulanKuesionerByUsia(umur)

    override fun getRecordBb(): LiveData<List<RecordBb>> = localDataSource.getRecordBb()

    override fun getRecordBbByUsia(umur: Int): LiveData<RecordBb> = localDataSource.getRecordBbByUsia(umur)

    override fun insertRecordBb(data: RecordBb) {
        CoroutineScope(Dispatchers.IO).launch {
            localDataSource.insertRecordBb(data)
        }
    }

    override fun getRecordTb(): LiveData<List<RecordTb>> = localDataSource.getRecordTb()

    override fun getRecordTbByUsia(umur: Int): LiveData<RecordTb> = localDataSource.getRecordTbByUsia(umur)

    override fun insertRecordTb(data: RecordTb) {
        CoroutineScope(Dispatchers.IO).launch {
            localDataSource.insertRecordTb(data)
        }
    }

    override fun getBeratBadan(gender: Int): LiveData<List<BeratBadan>> = localDataSource.getBeratBadan(gender)

    override fun getKesimpulanBeratBadan(gender: Int, umur: Int): LiveData<BeratBadan> = localDataSource.getKesimpulanBeratBadan(gender, umur)

    override fun insertBeratBadan(beratBadan: BeratBadan) {
        CoroutineScope(Dispatchers.IO).launch {
            localDataSource.insertBeratBadan(beratBadan)
        }
    }

    override fun getTinggiBadan(gender: Int): LiveData<List<TinggiBadan>> = localDataSource.getTinggiBadan(gender)

    override fun getKesimpulanTinggiBadan(gender: Int, umur: Int): LiveData<TinggiBadan> = localDataSource.getKesimpulanTinggiBadan(gender, umur)

    override fun insertTinggiBadan(tinggiBadan: TinggiBadan) {
        CoroutineScope(Dispatchers.IO).launch {
            localDataSource.insertTinggiBadan(tinggiBadan)
        }
    }
}