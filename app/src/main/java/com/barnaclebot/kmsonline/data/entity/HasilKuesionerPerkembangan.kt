package com.barnaclebot.kmsonline.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "hasil_kuesioner_perkembangan")
data class HasilKuesionerPerkembangan(
    @PrimaryKey
    val umur: Int = 0,

    val skor: Int? = null,

    val tanggalRecord: String? = null
)