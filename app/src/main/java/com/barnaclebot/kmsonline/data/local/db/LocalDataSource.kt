package com.barnaclebot.kmsonline.data.local.db

import com.barnaclebot.kmsonline.data.entity.*
import javax.inject.Inject

class LocalDataSource @Inject constructor(private val databaseDao: DatabaseDao) {
    //user
    fun getUser() = databaseDao.getUser()

    suspend fun insertUser(user: User) = databaseDao.insertUser(user)

    suspend fun updateUser(idUser: Int, user: User) =
        databaseDao.updateUser(idUser, user.nama, user.tanggalLahir, user.jenisKelamin, user.alamat)

    suspend fun deleteUser() = databaseDao.deleteUser()

    //kuesioner perkembangan
    fun getKuesioner(umur: Int) = databaseDao.getKuesioner(umur)

    suspend fun insertKuesioner(kuesioner: List<KuesionerPerkembangan>) = databaseDao.insertKuesioner(kuesioner)

    suspend fun deleteKuesioner() = databaseDao.deleteKuesioner()

    //hasil kuesioner perkembangan
    suspend fun insertHasilKuesioner(data: HasilKuesionerPerkembangan) = databaseDao.insertHasilKuesioner(data)

    fun getKesimpulanKuesionerByUsia(umur: Int) = databaseDao.getKesimpulanKuesionerByUsia(umur)

    suspend fun deleteHasilKuesioner() = databaseDao.deletHasilKuesioner()

    //record bayi
    fun getRecordBb() = databaseDao.getRecordBb()

    fun getRecordBbByUsia(umur: Int) = databaseDao.getRecordBbByUsia(umur)

    suspend fun insertRecordBb(data: RecordBb) = databaseDao.insertRecordBb(data)

    suspend fun deleteRecordBb() = databaseDao.deleteRecordBb()

    fun getRecordTb() = databaseDao.getRecordTb()

    fun getRecordTbByUsia(umur: Int) = databaseDao.getRecordTbByUsia(umur)

    suspend fun insertRecordTb(data: RecordTb) = databaseDao.insertRecordTb(data)

    suspend fun deleteRecordTb() = databaseDao.deleteRecordTb()

    // berat badan
    fun getBeratBadan(gender: Int) = databaseDao.getBeratBadan(gender)

    fun getKesimpulanBeratBadan(gender: Int, umur: Int) = databaseDao.getKesimpulanBeratBadan(gender, umur)

    suspend fun insertBeratBadan(beratBadan: BeratBadan) = databaseDao.insertBeratBadan(beratBadan)

    suspend fun deleteBeratBadan() = databaseDao.deleteBeratBadan()

    // berat badan
    fun getTinggiBadan(gender: Int) = databaseDao.getTinggiBadan(gender)

    fun getKesimpulanTinggiBadan(gender: Int, umur: Int) = databaseDao.getKesimpulanTinggiBadan(gender, umur)

    suspend fun insertTinggiBadan(tinggiBadan: TinggiBadan) = databaseDao.insertTinggiBadan(tinggiBadan)

    suspend fun deleteTinggiBadan() = databaseDao.deleteTinggiBadan()
}