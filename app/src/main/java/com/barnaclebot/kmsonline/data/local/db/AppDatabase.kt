package com.barnaclebot.kmsonline.data.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.barnaclebot.kmsonline.data.entity.*

@Database(entities = [User::class, KuesionerPerkembangan::class, HasilKuesionerPerkembangan::class,
    RecordBb::class, RecordTb::class, BeratBadan::class, TinggiBadan::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun databaseDao(): DatabaseDao
}