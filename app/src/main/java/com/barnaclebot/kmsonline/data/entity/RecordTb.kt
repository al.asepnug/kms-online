package com.barnaclebot.kmsonline.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "record_tb")
data class RecordTb(
    @PrimaryKey
    val umur: Int,

    val tinggiBadan: Float,

    val tanggalRecord: String
)