package com.barnaclebot.kmsonline.data.local.prefs

import android.content.Context
import com.barnaclebot.kmsonline.data.entity.DataVersioning
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PrefsDataSource @Inject constructor(@ApplicationContext context: Context) {
    companion object {
        const val PREFERENCE_NAME = "KMS_ONLINE_PREF"
        const val IS_LOGIN = "is_login"
        const val KUESIONER_VERSION = "kuesioner_version"
        const val PERTUMBUHAN_VERSION = "pertumbuhan_version"
    }

    private val preferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)

    fun removeAllData() {
        val editor = preferences.edit()
        editor.clear()
        editor.apply()
    }

    fun setIsLogin(value: Boolean) {
        val editor = preferences.edit()
        editor.putBoolean(IS_LOGIN, value)
        editor.apply()
    }

    fun getIsLogin(): Boolean {
        return preferences.getBoolean(IS_LOGIN, false)
    }

    fun setKuesionerVersioning(value: Int) {
        val editor = preferences.edit()
        editor.putInt(KUESIONER_VERSION, value)
        editor.apply()
    }

    fun setPertumbuhanVersioning(value: Int) {
        val editor = preferences.edit()
        editor.putInt(PERTUMBUHAN_VERSION, value)
        editor.apply()
    }

    fun getKuesionerVersioning(): Int {
        return preferences.getInt(KUESIONER_VERSION, 0)
    }

    fun getPertumbuhanVersioning(): Int {
        return preferences.getInt(PERTUMBUHAN_VERSION, 0)
    }
}