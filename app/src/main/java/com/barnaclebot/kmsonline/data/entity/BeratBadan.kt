package com.barnaclebot.kmsonline.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.IgnoredOnParcel

@Entity(tableName = "berat_badan")
data class BeratBadan(
    val umur: Int,

    val sangatKurang: String,

    val kurang: String,

    val cukupKurang: String,

    val normal: String,

    val cukupLebih: String,

    val lebih: String,

    val sangatLebih: String,

    val gender: Int
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}