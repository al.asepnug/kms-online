package com.barnaclebot.kmsonline.data.entity

import com.google.gson.annotations.SerializedName

data class DataVersioning(

	@field:SerializedName("kuesionerVersion")
	val kuesionerVersion: Int? = null,

	@field:SerializedName("pertumbuhanVersion")
	val pertumbuhanVersion: Int? = null
)
