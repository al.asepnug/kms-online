package com.barnaclebot.kmsonline.data.local.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.barnaclebot.kmsonline.data.entity.*

@Dao
interface DatabaseDao {
    //user
    @Query("SELECT * FROM user LIMIT 1")
    fun getUser() : LiveData<User>

    @Insert(onConflict = OnConflictStrategy.REPLACE, entity = User::class)
    suspend fun insertUser(user: User)

    @Query("UPDATE user SET nama = :nama, tanggalLahir = :tanggalLahir, jenisKelamin = :jenisKelamin, alamat = :alamat WHERE id = :id")
    suspend fun updateUser(id: Int, nama: String, tanggalLahir: String, jenisKelamin: Int, alamat: String)

    @Query("DELETE FROM user")
    suspend fun deleteUser()

    //kuesioner perkembangan
    @Query("SELECT * FROM kuesioner_perkembangan WHERE :age BETWEEN rangeMin AND rangeMax")
    fun getKuesioner(age: Int) : LiveData<List<KuesionerPerkembangan>>

    @Insert(onConflict = OnConflictStrategy.REPLACE, entity = KuesionerPerkembangan::class)
    suspend fun insertKuesioner(kuesionerPerkembangan: List<KuesionerPerkembangan>)

    @Query("DELETE FROM kuesioner_perkembangan")
    suspend fun deleteKuesioner()

    //hasil kuesioner perkembangan
    @Insert(onConflict = OnConflictStrategy.REPLACE, entity = HasilKuesionerPerkembangan::class)
    suspend fun insertHasilKuesioner(data: HasilKuesionerPerkembangan)

    @Query("SELECT * FROM hasil_kuesioner_perkembangan WHERE umur = :umur LIMIT 1")
    fun getKesimpulanKuesionerByUsia(umur: Int) : LiveData<HasilKuesionerPerkembangan>

    @Query("DELETE FROM hasil_kuesioner_perkembangan")
    suspend fun deletHasilKuesioner()

    //record bb
    @Query("SELECT * FROM record_bb ORDER BY umur ASC")
    fun getRecordBb() : LiveData<List<RecordBb>>

    @Query("SELECT * FROM record_bb WHERE umur = :usia LIMIT 1")
    fun getRecordBbByUsia(usia: Int) : LiveData<RecordBb>

    @Insert(onConflict = OnConflictStrategy.REPLACE, entity = RecordBb::class)
    suspend fun insertRecordBb(data: RecordBb)

    @Query("DELETE FROM record_bb")
    suspend fun deleteRecordBb()

    //record tb
    @Query("SELECT * FROM record_tb ORDER BY umur ASC")
    fun getRecordTb() : LiveData<List<RecordTb>>

    @Query("SELECT * FROM record_tb WHERE umur = :usia LIMIT 1")
    fun getRecordTbByUsia(usia: Int) : LiveData<RecordTb>

    @Insert(onConflict = OnConflictStrategy.REPLACE, entity = RecordTb::class)
    suspend fun insertRecordTb(data: RecordTb)

    @Query("DELETE FROM record_tb")
    suspend fun deleteRecordTb()

    //berat badan
    @Query("SELECT * FROM berat_badan WHERE gender = :gender ORDER BY umur ASC")
    fun getBeratBadan(gender: Int) : LiveData<List<BeratBadan>>

    @Query("SELECT * FROM berat_badan WHERE umur = :umur AND gender = :gender LIMIT 1")
    fun getKesimpulanBeratBadan(gender: Int, umur: Int) : LiveData<BeratBadan>

    @Insert(onConflict = OnConflictStrategy.REPLACE, entity = BeratBadan::class)
    suspend fun insertBeratBadan(beratBadan: BeratBadan)

    @Query("DELETE FROM berat_badan")
    suspend fun deleteBeratBadan()

    //tinggi badan
    @Query("SELECT * FROM tinggi_badan WHERE gender = :gender ORDER BY umur ASC")
    fun getTinggiBadan(gender: Int) : LiveData<List<TinggiBadan>>

    @Query("SELECT * FROM tinggi_badan WHERE umur = :umur AND gender = :gender LIMIT 1")
    fun getKesimpulanTinggiBadan(gender: Int, umur: Int) : LiveData<TinggiBadan>

    @Insert(onConflict = OnConflictStrategy.REPLACE, entity = TinggiBadan::class)
    suspend fun insertTinggiBadan(tinggiBadan: TinggiBadan)

    @Query("DELETE FROM tinggi_badan")
    suspend fun deleteTinggiBadan()
}