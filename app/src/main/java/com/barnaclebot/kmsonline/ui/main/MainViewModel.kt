package com.barnaclebot.kmsonline.ui.main

import androidx.lifecycle.ViewModel
import com.barnaclebot.kmsonline.data.DataRepository
import com.barnaclebot.kmsonline.data.entity.BeratBadan
import com.barnaclebot.kmsonline.data.entity.TinggiBadan
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val dataRepository: DataRepository
) : ViewModel() {
    fun getUser() = dataRepository.getUser()

    fun getHasilKuesionerByUsia(umur: Int) = dataRepository.getHasilKuesionerByUsia(umur)

    fun getKesimpulanBeratBadan(gender: Int, umur: Int) = dataRepository.getKesimpulanBeratBadan(gender, umur)

    fun getKesimpulanTinggiBadan(gender: Int, umur: Int) = dataRepository.getKesimpulanTinggiBadan(gender, umur)

    fun getPertumbuhanVersioning(): Int = dataRepository.getPertumbuhanVersioning()

    fun insertPertumbuhanVersioning(dataVersioning: Int) = dataRepository.setPertumbuhanVersioning(dataVersioning)

    fun insertBeratBadan(data: BeratBadan) = dataRepository.insertBeratBadan(data)

    fun insertTinggiBadan(data: TinggiBadan) = dataRepository.insertTinggiBadan(data)

    fun getRecordBbByUsia(usia: Int) = dataRepository.getRecordBbByUsia(usia)

    fun getRecordTbByUsia(usia: Int) = dataRepository.getRecordTbByUsia(usia)

}