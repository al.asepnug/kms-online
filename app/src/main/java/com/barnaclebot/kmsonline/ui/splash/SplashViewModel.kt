package com.barnaclebot.kmsonline.ui.splash

import androidx.lifecycle.ViewModel
import com.barnaclebot.kmsonline.data.DataRepository
import com.barnaclebot.kmsonline.data.entity.DataVersioning
import com.barnaclebot.kmsonline.data.entity.KuesionerPerkembangan
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(
    private val dataRepository: DataRepository
) : ViewModel(){

    fun getIsLogin(): Boolean = dataRepository.getIsLogin()
}