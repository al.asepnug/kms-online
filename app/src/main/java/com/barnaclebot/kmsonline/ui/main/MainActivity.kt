package com.barnaclebot.kmsonline.ui.main

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import cn.pedant.SweetAlert.SweetAlertDialog
import com.barnaclebot.kmsonline.R
import com.barnaclebot.kmsonline.data.entity.BeratBadan
import com.barnaclebot.kmsonline.data.entity.TinggiBadan
import com.barnaclebot.kmsonline.data.entity.User
import com.barnaclebot.kmsonline.databinding.ActivityMainBinding
import com.barnaclebot.kmsonline.ui.grafik.GrafikActivity
import com.barnaclebot.kmsonline.ui.kuesioner.KuesionerActivity
import com.barnaclebot.kmsonline.ui.profil.ProfileActivity
import com.barnaclebot.kmsonline.utils.*
import com.barnaclebot.kmsonline.utils.AgeCalculator.calculateAge
import com.barnaclebot.kmsonline.utils.AgeCalculator.calculateAgeInMonths
import com.barnaclebot.kmsonline.utils.AgeCalculator.parseDate
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.json.JSONObject

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding: ActivityMainBinding
    private val viewModel: MainViewModel by viewModels()
    private lateinit var dataUser: User
    private lateinit var pDialog: SweetAlertDialog
    private var ageMonth : Int = 0
    private var height: Float = 0F
    private var weight: Float = 0F

    companion object {
        const val DATA = "data_user"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        pDialog = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
        binding.viewKuesioner.setOnClickListener(this)
        binding.viewGrafik.setOnClickListener(this)
        binding.viewProfile.setOnClickListener(this)
        checkVersion()
        populateUser()
    }

    @SuppressLint("SetTextI18n")
    private fun populateUser() {
        viewModel.getUser().observe(this, { result ->
            if (result != null) {
                dataUser = result
                binding.tvName.text = "Hi, ${dataUser.nama}"
                binding.tvBorn.text = dataUser.tanggalLahir
                val age: Age = calculateAge(parseDate(dataUser.tanggalLahir))
                if (age.years == 0) {
                    binding.tvAge.text = "${age.months} bln ${age.days} hari"
                } else {
                    binding.tvAge.text = "${age.years} th ${age.months} bln ${age.days} hari"
                }
                ageMonth = calculateAgeInMonths(age)
                populateRecord()
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun populateRecord() {
        viewModel.getRecordBbByUsia(ageMonth).observe(this, { result ->
            if (result != null) {
                binding.tvWeight.text = "${result.beratBadan} Kg"
                weight = result.beratBadan
                viewModel.getKesimpulanBeratBadan(dataUser.jenisKelamin, ageMonth).observe(this, {
                    if (it != null) {
                        when {
                            weight < it.sangatKurang.toFloat() -> binding.tvRingkasanPertumbuhanBb.text =
                                "Pertumbuhan berat badan sangat kurang"
                            weight < it.kurang.toFloat() -> binding.tvRingkasanPertumbuhanBb.text =
                                "Pertumbuhan berat badan kurang"
                            weight < it.cukupLebih.toFloat() -> binding.tvRingkasanPertumbuhanBb.text =
                                "Pertumbuhan berat badan normal"
                            else -> binding.tvRingkasanPertumbuhanBb.text = "Pertumbuhan berat badan lebih"
                        }
                    } else {
                        binding.tvRingkasanPertumbuhanBb.text = "Berat badan bulan ini belum di isi"
                    }
                })
            } else {
                binding.tvWeight.text = "- Kg"
                makeToast("berat badan bulan ini kosong")
            }
        })

        viewModel.getRecordTbByUsia(ageMonth).observe(this, { result ->
            if (result != null) {
                binding.tvHeight.text = "${result.tinggiBadan} Cm"
                height = result.tinggiBadan
                viewModel.getKesimpulanTinggiBadan(dataUser.jenisKelamin,ageMonth).observe(this, {
                    if (it != null) {
                        when {
                            height >= it.cukupLebih.toFloat() -> binding.tvRingkasanPertumbuhanTb.text =
                                "Pertumbuhan tinggi badan tinggi"
                            height >= it.cukupKurang.toFloat() -> binding.tvRingkasanPertumbuhanTb.text =
                                "Pertumbuhan tinggi badan normal"
                            height >= it.sangatKurang.toFloat() -> binding.tvRingkasanPertumbuhanTb.text =
                                "Pertumbuhan tinggi badan pendek"
                            else -> binding.tvRingkasanPertumbuhanTb.text =
                                "Pertumbuhan tinggi badan sangat pendek"
                        }
                    } else {
                        binding.tvRingkasanPertumbuhanBb.text = "Tinggi badan bulan ini belum di isi"
                    }
                })
            } else {
                binding.tvHeight.text = "- Cm"
                makeToast("tinggi badan bulan ini kosong")
            }
        })

        initRingkasan()
    }

    @SuppressLint("SetTextI18n")
    private fun initRingkasan() {
        viewModel.getHasilKuesionerByUsia(ageMonth).observe(this, {
                if (it != null) {
                    when (it.skor) {
                        in 9..20 -> {
                            binding.tvRingkasanPerkembangan.text = "Perkembangan Bayi Normal"
                        }
                        in 7..8 -> {
                            binding.tvRingkasanPerkembangan.text = "Perkembangan Bayi Meragukan"
                        }
                        else -> {
                            binding.tvRingkasanPerkembangan.text = "Perkembangan Bayi Menyimpang"
                        }
                    }
                } else {
                    binding.tvRingkasanPerkembangan.text = "Kueisoner Perkembangan Bayi Bulan ini belum di isi"
                }
            })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.viewKuesioner -> {
                val intent = Intent(this, KuesionerActivity::class.java)
                startActivity(intent)
            }

            R.id.viewGrafik -> {
                val intent = Intent(this, GrafikActivity::class.java)
                intent.putExtra(DATA, dataUser)
                startActivity(intent)
            }

            R.id.viewProfile -> {
                val intent = Intent(this, ProfileActivity::class.java)
                startActivity(intent)
            }
        }
    }

    private fun checkVersion() {
        try {
            val jsonFileString = ValueFormat.getJsonDataFromAsset(this, "dataVersioning.json")
            if (jsonFileString != null) {
                val jsonObject = JSONObject(jsonFileString)
                if (jsonObject.getInt("pertumbuhanVersion") != viewModel.getPertumbuhanVersioning()) {
                    makeLoading(pDialog, true)
                    try {
                        GlobalScope.launch(Dispatchers.IO) {
                            insertDataBeratFromCsv()
                        }
                        GlobalScope.launch(Dispatchers.IO) {
                            insertDataTinggiFromCsv()
                        }
                        viewModel.insertPertumbuhanVersioning(jsonObject.getInt("pertumbuhanVersion"))
                        makeLoading(pDialog, false)
                    } catch (e: Exception) {
                        makeLoading(pDialog, false)
                        makeToast("Reading CSV Error!")
                        e.printStackTrace()
                    }
                }
            }
        } catch (e: Exception) {
            makeLoading(pDialog, false)
            makeToast("Reading JSON Error!")
            e.printStackTrace()
        }
    }

    private fun insertDataBeratFromCsv() {
        // berat badan laki
        try {
            val dataCsv = ValueFormat.getCsvDataFromAsset(this, "bbLaki.csv")
            if (dataCsv != null) {
                for (data in dataCsv) {
                    val tokens = data.split(";")
                    viewModel.insertBeratBadan(
                        BeratBadan(
                            tokens[0].toInt(),
                            tokens[1],
                            tokens[2],
                            tokens[3],
                            tokens[4],
                            tokens[5],
                            tokens[6],
                            tokens[7],
                            0
                        )
                    )
                }
            }
        } catch (e: Exception) {
            Log.d("grafikerror", e.toString())
            e.printStackTrace()
        }

        try {
            val dataCsv = ValueFormat.getCsvDataFromAsset(this, "bbPerempuan.csv")
            if (dataCsv != null) {
                for (data in dataCsv) {
                    val tokens = data.split(";")
                    viewModel.insertBeratBadan(
                        BeratBadan(
                            tokens[0].toInt(),
                            tokens[1],
                            tokens[2],
                            tokens[3],
                            tokens[4],
                            tokens[5],
                            tokens[6],
                            tokens[7],
                            1
                        )
                    )
                }
            }
        } catch (e: Exception) {
            Log.d("grafikerror", e.toString())
            e.printStackTrace()
        }
    }

    private fun insertDataTinggiFromCsv() {
        // tinggi badan laki
        try {
            val dataCsv = ValueFormat.getCsvDataFromAsset(this, "tbLaki.csv")
            if (dataCsv != null) {
                for (data in dataCsv) {
                    val tokens = data.split(";")
                    viewModel.insertTinggiBadan(
                        TinggiBadan(
                            tokens[0].toInt(),
                            tokens[1],
                            tokens[2],
                            tokens[3],
                            tokens[4],
                            tokens[5],
                            tokens[6],
                            tokens[7],
                            0
                        )
                    )
                }
            }
        } catch (e: Exception) {
            Log.d("grafikerror", e.toString())
            e.printStackTrace()
        }

        // Tinggi badan perempuan
        try {
            val dataCsv = ValueFormat.getCsvDataFromAsset(this, "tbPerempuan.csv")
            if (dataCsv != null) {
                for (data in dataCsv) {
                    val tokens = data.split(";")
                    viewModel.insertTinggiBadan(
                        TinggiBadan(
                            tokens[0].toInt(),
                            tokens[1],
                            tokens[2],
                            tokens[3],
                            tokens[4],
                            tokens[5],
                            tokens[6],
                            tokens[7],
                            1
                        )
                    )
                }
            }
        } catch (e: Exception) {
            Log.d("grafikerror", e.toString())
            e.printStackTrace()
        }
    }
}