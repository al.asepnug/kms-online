package com.barnaclebot.kmsonline.ui.kuesioner

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.barnaclebot.kmsonline.data.entity.KuesionerPerkembangan
import com.barnaclebot.kmsonline.databinding.ItemKuesionerBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

class KuesionerAdapter(private val kuesionerListItem: ArrayList<KuesionerPerkembangan>,
                       private val yesListener: (KuesionerPerkembangan) -> Unit,
                       private val noListener: () -> Unit)
    : RecyclerView.Adapter<KuesionerAdapter.ViewHolder>(){

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemKuesionerBinding.inflate(LayoutInflater.from(viewGroup.context),
            viewGroup, false)
        return ViewHolder(
            binding
        )
    }

    override fun getItemCount(): Int {
        return kuesionerListItem.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(kuesionerListItem[position])
    }

    inner class ViewHolder(private val binding: ItemKuesionerBinding) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(data: KuesionerPerkembangan) {
            binding.tvTitle.text = "Pertanyaan ${(adapterPosition.plus(1))}"
            binding.tvKuesioner.text = data.soal
            Glide.with(itemView)
                .load(data.image)
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.imgKuesioner)

            binding.btnYa.setOnClickListener{
                yesListener(data)
            }

            binding.btnTidak.setOnClickListener{
                noListener()
            }
        }
    }

    fun updateData(newList: List<KuesionerPerkembangan>) {
        kuesionerListItem.clear()
        kuesionerListItem.addAll(newList)
        notifyDataSetChanged()
    }
}