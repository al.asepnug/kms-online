package com.barnaclebot.kmsonline.ui.grafik

import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import cn.pedant.SweetAlert.SweetAlertDialog
import com.barnaclebot.kmsonline.R
import com.barnaclebot.kmsonline.data.entity.*
import com.barnaclebot.kmsonline.databinding.ActivityGrafikBinding
import com.barnaclebot.kmsonline.ui.main.MainActivity
import com.barnaclebot.kmsonline.utils.ValueFormat
import com.barnaclebot.kmsonline.utils.ValueFormat.format2digit
import com.barnaclebot.kmsonline.utils.makeToast
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.DefaultValueFormatter
import com.github.mikephil.charting.formatter.LargeValueFormatter
import com.google.android.material.button.MaterialButton
import com.google.android.material.datepicker.MaterialDatePicker
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException

@AndroidEntryPoint
class GrafikActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding: ActivityGrafikBinding
    private val viewModel: GrafikViewModel by viewModels()
    private lateinit var pDialog: SweetAlertDialog
    private lateinit var dataUser: User
    private var bbSangatKurang = ArrayList<Entry>()
    private var bbKurang = ArrayList<Entry>()
    private var bbCukupKurang = ArrayList<Entry>()
    private var bbNormal = ArrayList<Entry>()
    private var bbCukupLebih = ArrayList<Entry>()
    private var bbLebih = ArrayList<Entry>()
    private var bbSangatLebih = ArrayList<Entry>()
    private var tbSangatKurang = ArrayList<Entry>()
    private var tbkurang = ArrayList<Entry>()
    private var tbCukupKurang = ArrayList<Entry>()
    private var tbNormal = ArrayList<Entry>()
    private var tbCukupLebih = ArrayList<Entry>()
    private var tbLebih = ArrayList<Entry>()
    private var tbSangatLebih = ArrayList<Entry>()
    private lateinit var bbSangatKurangDataSet: LineDataSet
    private lateinit var bbkurangDataSet: LineDataSet
    private lateinit var bbCukupKurangDataSet: LineDataSet
    private lateinit var bbNormalKurangDataSet: LineDataSet
    private lateinit var bbCukupLebihDataSet: LineDataSet
    private lateinit var bbLebihDataSet: LineDataSet
    private lateinit var bbSangatLebihDataSet: LineDataSet
    private lateinit var tbSangatKurangDataSet: LineDataSet
    private lateinit var tbkurangDataSet: LineDataSet
    private lateinit var tbCukupKurangDataSet: LineDataSet
    private lateinit var tbNormalDataSet: LineDataSet
    private lateinit var tbCukupLebihDataSet: LineDataSet
    private lateinit var tbLebihDataSet: LineDataSet
    private lateinit var tbSangatLebihDataSet: LineDataSet

    private var recordBayi = ArrayList<Entry>()
    private lateinit var recordBayiDataSet: LineDataSet
    private var recordBayiTb = ArrayList<Entry>()
    private lateinit var recordBayiTbDataSet: LineDataSet

    private val listRecordBayi= ArrayList<RecordBb>()
    private val listRecordTb= ArrayList<RecordTb>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGrafikBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.title = "Grafik Pertumbuhan"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        pDialog = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
        binding.btnTambahBb.setOnClickListener(this)
        binding.btnTambahBbHeight.setOnClickListener(this)

        initUi()
        initChartBb()
        initChartTb()
        populateUser()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun initUi() {
        binding.rvBeratBadan.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = TabelBbAdapter(listRecordBayi)
        }
        binding.rvTinggiBadan.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = TabelTbAdapter(listRecordTb)
        }
    }

    private fun initChartBb() {
        bbSangatKurangDataSet = LineDataSet(bbSangatKurang, "bbSangatKurang")
        bbSangatKurangDataSet.mode = LineDataSet.Mode.LINEAR
        bbSangatKurangDataSet.color = Color.TRANSPARENT
        bbSangatKurangDataSet.fillAlpha = 255
        bbSangatKurangDataSet.setDrawFilled(true)
        bbSangatKurangDataSet.setDrawValues(false)
        bbSangatKurangDataSet.fillColor = Color.parseColor("#FF7474")
        bbSangatKurangDataSet.setDrawCircles(false)
        bbSangatKurangDataSet.setDrawValues(false)
        bbSangatKurangDataSet.setFillFormatter { dataSet, dataProvider ->
            binding.chartWeight.axisLeft.axisMaximum
        }
        bbkurangDataSet = LineDataSet(bbKurang, "bbKurang")
//        bbkurangDataSet.mode = LineDataSet.Mode.LINEAR
        bbkurangDataSet.color = Color.TRANSPARENT
        bbkurangDataSet.fillAlpha = 255
        bbkurangDataSet.setDrawFilled(true)
        bbkurangDataSet.setDrawValues(false)
        bbkurangDataSet.fillColor = Color.parseColor("#FFFF5E")
        bbkurangDataSet.setDrawCircles(false)
        bbkurangDataSet.setFillFormatter { dataSet, dataProvider ->
            binding.chartWeight.axisLeft.axisMaximum
        }

        bbCukupKurangDataSet = LineDataSet(bbCukupKurang, "bbCukupKurang")
//        bbCukupKurangDataSet.mode = LineDataSet.Mode.LINEAR
        bbCukupKurangDataSet.color = Color.TRANSPARENT
        bbCukupKurangDataSet.fillAlpha = 255
        bbCukupKurangDataSet.setDrawFilled(true)
        bbCukupKurangDataSet.setDrawValues(false)
        bbCukupKurangDataSet.fillColor = Color.parseColor("#7DFF7D")
        bbCukupKurangDataSet.setDrawCircles(false)
        bbCukupKurangDataSet.setFillFormatter { dataSet, dataProvider ->
            binding.chartWeight.axisLeft.axisMaximum
        }

        bbNormalKurangDataSet = LineDataSet(bbNormal, "bbNormal")
//        bbNormalKurangDataSet.mode = LineDataSet.Mode.LINEAR
        bbNormalKurangDataSet.color = Color.TRANSPARENT
        bbNormalKurangDataSet.fillAlpha = 255
        bbNormalKurangDataSet.setDrawFilled(true)
        bbNormalKurangDataSet.setDrawValues(false)
        bbNormalKurangDataSet.fillColor = Color.parseColor("#7DFF7D")
        bbNormalKurangDataSet.setDrawCircles(false)
        bbNormalKurangDataSet.setFillFormatter { dataSet, dataProvider ->
            binding.chartWeight.axisLeft.axisMaximum
        }

        bbCukupLebihDataSet = LineDataSet(bbCukupLebih, "bbCukupLebih")
//        bbCukupLebihDataSet.mode = LineDataSet.Mode.LINEAR
        bbCukupLebihDataSet.color = Color.TRANSPARENT
        bbCukupLebihDataSet.fillAlpha = 255
        bbCukupLebihDataSet.setDrawFilled(true)
        bbCukupLebihDataSet.setDrawValues(false)
        bbCukupLebihDataSet.fillColor = Color.parseColor("#FFFF5E")
        bbCukupLebihDataSet.setDrawCircles(false)
        bbCukupLebihDataSet.setFillFormatter { dataSet, dataProvider ->
            binding.chartWeight.axisLeft.axisMaximum
        }

        bbLebihDataSet = LineDataSet(bbLebih, "bbLebih")
//        bbLebihDataSet.mode = LineDataSet.Mode.LINEAR
        bbLebihDataSet.color = Color.TRANSPARENT
        bbLebihDataSet.fillAlpha = 255
        bbLebihDataSet.setDrawFilled(true)
        bbLebihDataSet.setDrawValues(false)
        bbLebihDataSet.fillColor = Color.parseColor("#FF7474")
        bbLebihDataSet.setDrawCircles(false)
        bbLebihDataSet.setFillFormatter { dataSet, dataProvider ->
            binding.chartWeight.axisLeft.axisMaximum
        }

        bbSangatLebihDataSet = LineDataSet(bbSangatLebih, "bbSangatLebih")
        //bbSangatLebihDataSet.mode = LineDataSet.Mode.LINEAR
        bbSangatLebihDataSet.color = Color.TRANSPARENT
        bbSangatLebihDataSet.fillAlpha = 255
        bbSangatLebihDataSet.setDrawFilled(true)
        bbSangatLebihDataSet.setDrawValues(false)
        bbSangatLebihDataSet.fillColor = Color.WHITE
        bbSangatLebihDataSet.setDrawCircles(false)
        bbSangatLebihDataSet.setFillFormatter { dataSet, dataProvider ->
            binding.chartWeight.axisLeft.axisMaximum
        }
        // lebihLineDataSet.fillFormatter = MyFillFormatter(cukupLebihLineDataSet)
        recordBayiDataSet = LineDataSet(recordBayi, "recordBayi")
        recordBayiDataSet.mode = LineDataSet.Mode.CUBIC_BEZIER
        recordBayiDataSet.color = Color.BLACK
        recordBayiDataSet.setDrawFilled(false)
        recordBayiDataSet.setDrawValues(true)
        recordBayiDataSet.setDrawCircles(true)
        recordBayiDataSet.setDrawCircleHole(false)
        recordBayiDataSet.setCircleColor(Color.parseColor("#1D2E6A"))
        recordBayiDataSet.isDrawValuesEnabled
        recordBayiDataSet.valueTextColor = Color.BLACK
        recordBayiDataSet.valueTextSize = 15.0F
        recordBayiDataSet.valueFormatter = DefaultValueFormatter(1)
//        recordBayiDataSet.setFillFormatter { dataSet, dataProvider ->
//            binding.chartWeight.axisLeft.axisMaximum
//        }

        //Setup Legend
        val legend = binding.chartWeight.legend
        legend.isEnabled = false

        val xAxis: XAxis = binding.chartWeight.xAxis
        xAxis.isEnabled = true
        xAxis.textColor = Color.BLACK
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.textSize = 10f
        xAxis.setDrawGridLines(false)
        xAxis.setDrawAxisLine(true)
        xAxis.setAvoidFirstLastClipping(true)
        xAxis.isGranularityEnabled = true
        xAxis.granularity = 1f

        val leftAxis: YAxis = binding.chartWeight.axisLeft
        leftAxis.isEnabled = true
        leftAxis.textColor = Color.BLACK
//        leftAxis.setAxisMaximum(400f);
//        leftAxis.setAxisMinimum(0f);
        //        leftAxis.setAxisMaximum(400f);
//        leftAxis.setAxisMinimum(0f);
        leftAxis.setDrawGridLines(false)
        leftAxis.textSize = 10f
        leftAxis.setDrawLabels(true)
        leftAxis.setDrawAxisLine(true)
//        leftAxis.setGridColor(ContextCompat.getColor(mContext, R.color.transparent30));
        //        leftAxis.setGridColor(ContextCompat.getColor(mContext, R.color.transparent30));
        val lFormat = LargeValueFormatter()
        leftAxis.valueFormatter = lFormat

        val rightAxis: YAxis = binding.chartWeight.axisRight
        rightAxis.isEnabled = false

        binding.chartWeight.description.isEnabled = false
        binding.chartWeight.data = LineData(
            bbSangatKurangDataSet,
            bbkurangDataSet,
            bbCukupKurangDataSet,
            bbNormalKurangDataSet,
            bbCukupLebihDataSet,
            bbLebihDataSet,
            bbSangatLebihDataSet,
            recordBayiDataSet
        )
//        binding.chartWeight.renderer = MyLineLegendRenderer(
//            binding.chartWeight,
//            binding.chartWeight.animator,
//            binding.chartWeight.viewPortHandler
//        )

//        val marker: IMarker = CustomMarkerView(this, R.layout.custom_marker_view)
//        binding.chartWeight.markerView = marker
        binding.chartWeight.invalidate()
    }

    private fun initChartTb() {
        tbSangatKurangDataSet = LineDataSet(tbSangatKurang, "tbSangatKurang")
        tbSangatKurangDataSet.mode = LineDataSet.Mode.LINEAR
        tbSangatKurangDataSet.color = Color.TRANSPARENT
        tbSangatKurangDataSet.fillAlpha = 255
        tbSangatKurangDataSet.setDrawFilled(true)
        tbSangatKurangDataSet.setDrawValues(false)
        tbSangatKurangDataSet.fillColor = Color.parseColor("#FF7474")
        tbSangatKurangDataSet.setDrawCircles(false)
        tbSangatKurangDataSet.setDrawValues(false)
        tbSangatKurangDataSet.setFillFormatter { dataSet, dataProvider ->
            binding.chartHeight.axisLeft.axisMaximum
        }
        tbkurangDataSet = LineDataSet(tbkurang, "bbKurang")
//        bbkurangDataSet.mode = LineDataSet.Mode.LINEAR
        tbkurangDataSet.color = Color.TRANSPARENT
        tbkurangDataSet.fillAlpha = 255
        tbkurangDataSet.setDrawFilled(true)
        tbkurangDataSet.setDrawValues(false)
        tbkurangDataSet.fillColor = Color.parseColor("#FFFF5E")
        tbkurangDataSet.setDrawCircles(false)
        tbkurangDataSet.setFillFormatter { dataSet, dataProvider ->
            binding.chartHeight.axisLeft.axisMaximum
        }

        tbCukupKurangDataSet = LineDataSet(tbCukupKurang, "tbCukupKurang")
//        bbCukupKurangDataSet.mode = LineDataSet.Mode.LINEAR
        tbCukupKurangDataSet.color = Color.TRANSPARENT
        tbCukupKurangDataSet.fillAlpha = 255
        tbCukupKurangDataSet.setDrawFilled(true)
        tbCukupKurangDataSet.setDrawValues(false)
        tbCukupKurangDataSet.fillColor = Color.parseColor("#7DFF7D")
        tbCukupKurangDataSet.setDrawCircles(false)
        tbCukupKurangDataSet.setFillFormatter { dataSet, dataProvider ->
            binding.chartHeight.axisLeft.axisMaximum
        }

        tbNormalDataSet = LineDataSet(tbNormal, "tbNormal")
//        bbNormalKurangDataSet.mode = LineDataSet.Mode.LINEAR
        tbNormalDataSet.color = Color.TRANSPARENT
        tbNormalDataSet.fillAlpha = 255
        tbNormalDataSet.setDrawFilled(true)
        tbNormalDataSet.setDrawValues(false)
        tbNormalDataSet.fillColor = Color.parseColor("#7DFF7D")
        tbNormalDataSet.setDrawCircles(false)
        tbNormalDataSet.setFillFormatter { dataSet, dataProvider ->
            binding.chartHeight.axisLeft.axisMaximum
        }

        tbCukupLebihDataSet = LineDataSet(tbCukupLebih, "tbCukupLebih")
//        bbCukupLebihDataSet.mode = LineDataSet.Mode.LINEAR
        tbCukupLebihDataSet.color = Color.TRANSPARENT
        tbCukupLebihDataSet.fillAlpha = 255
        tbCukupLebihDataSet.setDrawFilled(true)
        tbCukupLebihDataSet.setDrawValues(false)
        tbCukupLebihDataSet.fillColor = Color.parseColor("#FFFF5E")
        tbCukupLebihDataSet.setDrawCircles(false)
        tbCukupLebihDataSet.setFillFormatter { dataSet, dataProvider ->
            binding.chartHeight.axisLeft.axisMaximum
        }

        tbLebihDataSet = LineDataSet(tbLebih, "tbLebih")
//        bbLebihDataSet.mode = LineDataSet.Mode.LINEAR
        tbLebihDataSet.color = Color.TRANSPARENT
        tbLebihDataSet.fillAlpha = 255
        tbLebihDataSet.setDrawFilled(true)
        tbLebihDataSet.setDrawValues(false)
        tbLebihDataSet.fillColor = Color.parseColor("#FF7474")
        tbLebihDataSet.setDrawCircles(false)
        tbLebihDataSet.setFillFormatter { dataSet, dataProvider ->
            binding.chartHeight.axisLeft.axisMaximum
        }

        tbSangatLebihDataSet = LineDataSet(tbSangatLebih, "tbSangatLebih")
        //bbSangatLebihDataSet.mode = LineDataSet.Mode.LINEAR
        tbSangatLebihDataSet.color = Color.TRANSPARENT
        tbSangatLebihDataSet.fillAlpha = 255
        tbSangatLebihDataSet.setDrawFilled(true)
        tbSangatLebihDataSet.setDrawValues(false)
        tbSangatLebihDataSet.fillColor = Color.WHITE
        tbSangatLebihDataSet.setDrawCircles(false)
        tbSangatLebihDataSet.setFillFormatter { dataSet, dataProvider ->
            binding.chartHeight.axisLeft.axisMaximum
        }
        // lebihLineDataSet.fillFormatter = MyFillFormatter(cukupLebihLineDataSet)
        recordBayiTbDataSet = LineDataSet(recordBayiTb, "recordBayiTb")
        recordBayiTbDataSet.mode = LineDataSet.Mode.CUBIC_BEZIER
        recordBayiTbDataSet.color = Color.BLACK
        recordBayiTbDataSet.setDrawFilled(false)
        recordBayiTbDataSet.setDrawValues(true)
        recordBayiTbDataSet.setDrawCircles(true)
        recordBayiTbDataSet.setDrawCircleHole(false)
        recordBayiTbDataSet.setCircleColor(Color.parseColor("#1D2E6A"))
        recordBayiTbDataSet.valueTextColor = Color.BLACK
        recordBayiTbDataSet.valueTextSize = 15.0F
        recordBayiTbDataSet.valueFormatter = DefaultValueFormatter(1)
//        recordBayiDataSet.setFillFormatter { dataSet, dataProvider ->
//            binding.chartWeight.axisLeft.axisMaximum
//        }

        //Setup Legend
        val legend = binding.chartHeight.legend
        legend.isEnabled = false

        val xAxis: XAxis = binding.chartHeight.xAxis
        xAxis.isEnabled = true
        xAxis.textColor = Color.BLACK
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.textSize = 10f
        xAxis.setDrawGridLines(false)
        xAxis.setDrawAxisLine(true)
        xAxis.setAvoidFirstLastClipping(true)
        xAxis.isGranularityEnabled = true
        xAxis.granularity = 1f

        val leftAxis: YAxis = binding.chartHeight.axisLeft
        leftAxis.isEnabled = true
        leftAxis.textColor = Color.BLACK
//        leftAxis.setAxisMaximum(400f);
//        leftAxis.setAxisMinimum(0f);
        //        leftAxis.setAxisMaximum(400f);
//        leftAxis.setAxisMinimum(0f);
        leftAxis.setDrawGridLines(false)
        leftAxis.textSize = 10f
        leftAxis.setDrawLabels(true)
        leftAxis.setDrawAxisLine(true)
//        leftAxis.setGridColor(ContextCompat.getColor(mContext, R.color.transparent30));
        //        leftAxis.setGridColor(ContextCompat.getColor(mContext, R.color.transparent30));
        val lFormat = LargeValueFormatter()
        leftAxis.valueFormatter = lFormat

        val rightAxis: YAxis = binding.chartHeight.axisRight
        rightAxis.isEnabled = false

        binding.chartHeight.description.isEnabled = false
        binding.chartHeight.data = LineData(
            tbSangatKurangDataSet,
            tbkurangDataSet,
            tbCukupKurangDataSet,
            tbNormalDataSet,
            tbCukupLebihDataSet,
            tbLebihDataSet,
            tbSangatLebihDataSet,
            recordBayiTbDataSet
        )

//        val marker: IMarker = CustomMarkerView(this, R.layout.custom_marker_view)
//        binding.chartHeight.markerView = marker
        binding.chartHeight.invalidate()
    }

    private fun populateUser() {
        try {
            dataUser = intent.extras?.getParcelable(MainActivity.DATA)!!
            renderDataBb()
            renderDataTb()
        } catch (e: IOException) {
            makeToast("Data User Kosong")
        }
    }

    private fun renderDataBb() {
        viewModel.getBeratBadan(dataUser.jenisKelamin).observe(this, { result ->
            if (result != null) {
                for (data in result) {
                    Log.d("renderBb", data.umur.toString())
                    bbSangatKurang.add(
                        Entry(
                            data.umur.toFloat(),
                            data.sangatKurang.toFloat()
                        )
                    )
                    bbKurang.add(Entry(data.umur.toFloat(), data.kurang.toFloat()))
                    bbCukupKurang.add(
                        Entry(
                            data.umur.toFloat(),
                            data.cukupKurang.toFloat()
                        )
                    )
                    bbNormal.add(Entry(data.umur.toFloat(), data.normal.toFloat()))
                    bbCukupLebih.add(Entry(data.umur.toFloat(), data.cukupLebih.toFloat()))
                    bbLebih.add(Entry(data.umur.toFloat(), data.lebih.toFloat()))
                    bbSangatLebih.add(
                        Entry(
                            data.umur.toFloat(),
                            data.sangatLebih.toFloat()
                        )
                    )
                }
                updateBeratBadan()
            } else {
                makeToast("Data Master Berat Badan Kosong")
            }
        })
    }

    private fun renderDataTb() {
        viewModel.getTinggiBadan(dataUser.jenisKelamin).observe(this, { result ->
            if (result != null) {
                for (data in result) {
                    tbSangatKurang.add(Entry(data.umur.toFloat(), data.sangatKurang.toFloat()))
                    tbkurang.add(Entry(data.umur.toFloat(), data.kurang.toFloat()))
                    tbCukupKurang.add(Entry(data.umur.toFloat(), data.cukupKurang.toFloat()))
                    tbNormal.add(Entry(data.umur.toFloat(), data.normal.toFloat()))
                    tbCukupLebih.add(Entry(data.umur.toFloat(), data.cukupLebih.toFloat()))
                    tbLebih.add(Entry(data.umur.toFloat(), data.lebih.toFloat()))
                    tbSangatLebih.add(Entry(data.umur.toFloat(), data.sangatLebih.toFloat()))
                }
                updateTinggiBadan()
            } else {
                makeToast("Data Master Tinggi Badan Kosong")
            }
        })
    }

    private fun updateBeratBadan() {
        viewModel.getRecordBb().observe(this, { result ->
            if (result != null) {
                binding.rvBeratBadan.adapter?.let { adapter ->
                    when (adapter) {
                        is TabelBbAdapter -> {
                            try {
                                if (result.size > 0) {
                                    if (result.size > 10) {
                                        binding.tvGeser.visibility = View.VISIBLE
                                    }
                                    adapter.updateData(result)
                                } else {
                                    makeToast("Data is Empty")
                                }
                            } catch (e: IOException) {
                                makeToast("Error Load Data")
                            }
                        }
                    }
                }
                recordBayi.clear()
                recordBayiDataSet.clear()
                for (data in result) {
                    recordBayi.add(Entry(data.umur.toFloat(), data.beratBadan))
                }
                bbSangatKurangDataSet.notifyDataSetChanged()
                bbkurangDataSet.notifyDataSetChanged()
                bbCukupKurangDataSet.notifyDataSetChanged()
                bbNormalKurangDataSet.notifyDataSetChanged()
                bbCukupLebihDataSet.notifyDataSetChanged()
                bbLebihDataSet.notifyDataSetChanged()
                bbSangatLebihDataSet.notifyDataSetChanged()
                recordBayiDataSet.notifyDataSetChanged()
                binding.chartWeight.data.notifyDataChanged()
                binding.chartWeight.notifyDataSetChanged()
                binding.chartWeight.invalidate()
            } else {
                makeToast("Data Berat Badan Kosong")
            }
        })
    }

    private fun updateTinggiBadan() {
        viewModel.getRecordTb().observe(this, { result ->
            if (result != null) {
                binding.rvTinggiBadan.adapter?.let { adapter ->
                    when (adapter) {
                        is TabelTbAdapter -> {
                            try {
                                if (result.size > 0) {
                                    if (result.size > 10) {
                                        binding.tvGeserTb.visibility = View.VISIBLE
                                    }
                                    adapter.updateData(result)
                                } else {
                                    makeToast("Data is Empty")
                                }
                            } catch (e: IOException) {
                                makeToast("Error Load Data")
                            }
                        }
                    }
                }
                recordBayiTb.clear()
                recordBayiTbDataSet.clear()
                for (data in result) {
                    recordBayiTb.add(Entry(data.umur.toFloat(), data.tinggiBadan))
                }
                tbSangatKurangDataSet.notifyDataSetChanged()
                tbkurangDataSet.notifyDataSetChanged()
                tbCukupKurangDataSet.notifyDataSetChanged()
                tbNormalDataSet.notifyDataSetChanged()
                tbCukupLebihDataSet.notifyDataSetChanged()
                tbLebihDataSet.notifyDataSetChanged()
                tbSangatLebihDataSet.notifyDataSetChanged()
                recordBayiTbDataSet.notifyDataSetChanged()
                binding.chartHeight.data.notifyDataChanged()
                binding.chartHeight.notifyDataSetChanged()
                binding.chartHeight.invalidate()
            } else {
                makeToast("Data Berat Badan Kosong")
            }
        })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnTambahBb -> {
                showDialog(true)
            }

            R.id.btnTambahBbHeight -> {
                showDialog(false)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun showDialog(isDialogBb: Boolean) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_custom)
        val title = dialog.findViewById(R.id.tvTitle) as TextView
        val yesBtn = dialog.findViewById(R.id.btnSubmit) as MaterialButton
        val noBtn = dialog.findViewById(R.id.btnCancel) as MaterialButton
        val edtBeratBadan = dialog.findViewById(R.id.edtBeratBadan) as EditText
        val edtTglPenimbangan = dialog.findViewById(R.id.edtTglPenimbangan) as EditText
        val edtUmur = dialog.findViewById(R.id.edtBulan) as EditText

        val datePicker = MaterialDatePicker.Builder.datePicker()
            .setTitleText("Pilih Tanggal Penimbangan")
            .setSelection(MaterialDatePicker.todayInUtcMilliseconds())
            .build()
        datePicker.addOnNegativeButtonClickListener { datePicker.dismiss() }
        datePicker.addOnPositiveButtonClickListener {
            edtTglPenimbangan.setText(ValueFormat.convertLongToTime(it))
            datePicker.dismiss()
        }

        edtTglPenimbangan.setOnClickListener {
            datePicker.show(supportFragmentManager, datePicker.toString())
        }

        edtTglPenimbangan.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                datePicker.show(supportFragmentManager, datePicker.toString())
            } else {
                datePicker.dismiss()
            }
        }

        if (isDialogBb) {
            title.text = "Tambah / Ubah Berat Badan"
            edtBeratBadan.hint = "Berat badan (0.0)"
            edtTglPenimbangan.hint = "Tanggal Penimbangan"
        } else {
            title.text = "Tambah / Ubah Tinggi Badan"
            edtBeratBadan.hint = "Tinggi badan (0.0)"
            edtTglPenimbangan.hint = "Tanggal Pengukuran"
        }
        yesBtn.setOnClickListener {
            if (edtBeratBadan.text.toString().isEmpty() && edtTglPenimbangan.text.toString().isEmpty()
                && edtUmur.text.toString().isEmpty()) {
                makeToast("input tidak boleh kosong")
            } else {
                if (ValueFormat.validateValue(edtBeratBadan.text.toString())) {
                    if (isDialogBb) {
                        viewModel.insertRecordBb(
                            RecordBb(
                                edtUmur.text.toString().toInt(),
                                format2digit(edtBeratBadan.text.toString()),
                                edtTglPenimbangan.text.toString()
                            )
                        )
                    } else {
                        viewModel.insertRecordTb(RecordTb(edtUmur.text.toString().toInt(), format2digit(edtBeratBadan.text.toString()),
                            edtTglPenimbangan.text.toString()))
                    }
                } else {
                    makeToast("Format nilai berat / tinggi badan masih salah, pastikan tidak lebih 4 digit dan nilai maksimal 100")
                }
            }
            dialog.dismiss()
        }
        noBtn.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }
}