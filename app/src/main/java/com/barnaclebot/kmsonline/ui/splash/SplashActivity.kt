package com.barnaclebot.kmsonline.ui.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.text.method.LinkMovementMethod
import androidx.activity.viewModels
import com.barnaclebot.kmsonline.data.entity.DataVersioning
import com.barnaclebot.kmsonline.data.entity.KuesionerPerkembangan
import com.barnaclebot.kmsonline.databinding.ActivitySplashBinding
import com.barnaclebot.kmsonline.ui.main.MainActivity
import com.barnaclebot.kmsonline.ui.login.LoginActivity
import com.barnaclebot.kmsonline.utils.ValueFormat.getJsonDataFromAsset
import com.barnaclebot.kmsonline.utils.makeStatusBarTransparent
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.hilt.android.AndroidEntryPoint
import org.json.JSONObject
import java.util.*

@AndroidEntryPoint
class SplashActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySplashBinding
    private val viewModel: SplashViewModel by viewModels()
    lateinit var handler: Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        makeStatusBarTransparent()
        binding.tvCopyright.movementMethod = LinkMovementMethod.getInstance()
        handler = Handler()
        handler.postDelayed({
            checkIsLogin()
        }, 1000)
    }

    private fun checkIsLogin() {
        if (viewModel.getIsLogin()) {
            val intent = Intent(this@SplashActivity, MainActivity::class.java)
            startActivity(intent)
            finish()
        } else {
            val intent = Intent(this@SplashActivity, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
}