package com.barnaclebot.kmsonline.ui.profil

import androidx.lifecycle.ViewModel
import com.barnaclebot.kmsonline.data.DataRepository
import com.barnaclebot.kmsonline.data.entity.RecordBb
import com.barnaclebot.kmsonline.data.entity.RecordTb
import com.barnaclebot.kmsonline.data.entity.User
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val dataRepository: DataRepository
) : ViewModel() {
    fun getUser() = dataRepository.getUser()

    fun updateUser(id: Int, user: User) = dataRepository.updateUser(id, user)

    fun getRecordBbByUsia(usia: Int) = dataRepository.getRecordBbByUsia(usia)

    fun insertRecordBb(data: RecordBb) = dataRepository.insertRecordBb(data)

    fun getRecordTbByUsia(usia: Int) = dataRepository.getRecordTbByUsia(usia)

    fun insertRecordTb(data: RecordTb) = dataRepository.insertRecordTb(data)
}