package com.barnaclebot.kmsonline.ui.login

import androidx.lifecycle.ViewModel
import com.barnaclebot.kmsonline.data.DataRepository
import com.barnaclebot.kmsonline.data.entity.RecordBb
import com.barnaclebot.kmsonline.data.entity.RecordTb
import com.barnaclebot.kmsonline.data.entity.User
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val dataRepository: DataRepository
) : ViewModel() {

    fun insertUser(user: User) = dataRepository.insertUser(user)

    fun insertRecordBb(data: RecordBb) = dataRepository.insertRecordBb(data)

    fun insertRecordTb(data: RecordTb) = dataRepository.insertRecordTb(data)

    fun setIsLogin(boolean: Boolean) = dataRepository.setIsLogin(boolean)
}