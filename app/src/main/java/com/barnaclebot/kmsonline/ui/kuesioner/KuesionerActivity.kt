package com.barnaclebot.kmsonline.ui.kuesioner

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.barnaclebot.kmsonline.data.entity.HasilKuesionerPerkembangan
import com.barnaclebot.kmsonline.data.entity.KuesionerPerkembangan
import com.barnaclebot.kmsonline.data.entity.User
import com.barnaclebot.kmsonline.databinding.ActivityKuesionerBinding
import com.barnaclebot.kmsonline.ui.kesimpulan.KesimpulanActivity
import com.barnaclebot.kmsonline.ui.kesimpulan.KesimpulanActivity.Companion.DATA_SKOR
import com.barnaclebot.kmsonline.utils.AgeCalculator.calculateAge
import com.barnaclebot.kmsonline.utils.AgeCalculator.calculateAgeInMonths
import com.barnaclebot.kmsonline.utils.AgeCalculator.parseDate
import com.barnaclebot.kmsonline.utils.ValueFormat
import com.barnaclebot.kmsonline.utils.ValueFormat.formatDateNow
import com.barnaclebot.kmsonline.utils.makeToast
import com.google.android.material.tabs.TabLayoutMediator
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.hilt.android.AndroidEntryPoint
import org.json.JSONObject

@AndroidEntryPoint
class KuesionerActivity : AppCompatActivity() {
    private lateinit var binding: ActivityKuesionerBinding
    private val viewModel: KuesionerViewModel by viewModels()
    private val listKuesioner = ArrayList<KuesionerPerkembangan>()
    private lateinit var dataUser: User
    private var skor: Int = 1
    private var age: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityKuesionerBinding.inflate(layoutInflater)
        setContentView(binding.root)
        checkVersion()
    }

    private fun checkVersion() {
        try {
            val jsonFileString = ValueFormat.getJsonDataFromAsset(this, "dataVersioning.json")
            if (jsonFileString != null) {
                val jsonObject = JSONObject(jsonFileString)
                if (jsonObject.getInt("kuesionerVersion") == viewModel.getKuesionerVersioning()) {
                    initUi()
                    populateUser()
                } else {
                    viewModel.insertKuesionerVersioning(jsonObject.getInt("kuesionerVersion"))
                    insertDataKuesioner()
                }
            }
        } catch (e: Exception) {
            println("Reading JSON Error!")
            e.printStackTrace()
        }
    }

    private fun insertDataKuesioner() {
        val jsonFileString = ValueFormat.getJsonDataFromAsset(this, "kuesionerPerkembangan.json")
        if (jsonFileString != null) {
            val gson = Gson()
            val listKuesionerType = object : TypeToken<List<KuesionerPerkembangan>>() {}.type
            val kuesioners: List<KuesionerPerkembangan> =
                gson.fromJson(jsonFileString, listKuesionerType)
            viewModel.insertKuesioner(kuesioners)
            initUi()
            populateUser()
        }
    }

    private fun initUi() {
        binding.kuesioner.apply {
            adapter = KuesionerAdapter(kuesionerListItem = listKuesioner, yesListener = {
                val current: Int = currentItem + 1
                if (current < adapter!!.itemCount) {
                    skor++
                    binding.kuesioner.currentItem = current
                } else {
                    launchKesimpulan(skor)
                }
            }, noListener = {
                val current: Int = currentItem + 1
                if (current < adapter!!.itemCount) {
                    binding.kuesioner.currentItem = current
                } else {
                    launchKesimpulan(skor)
                }
            })
        }

        binding.kuesioner.clipToPadding = true
        binding.kuesioner.isUserInputEnabled = false
        //binding.banner.clipChildren = false
        TabLayoutMediator(binding.indicator, binding.kuesioner)
        { tab, position -> }.attach()
    }

    private fun populateUser() {
        viewModel.getUser().observe(this, { result ->
            if (result != null) {
                dataUser = result
                age = calculateAgeInMonths(calculateAge(parseDate(dataUser.tanggalLahir)))
                populateKuesioner(age)
            } else {
                makeToast("Data User Kosong")
            }
        })
    }

    private fun populateKuesioner(age: Int) {
        viewModel.getKuesioner(age).observe(this, { result ->
            if (result != null && result.isNotEmpty()) {
                binding.kuesioner.adapter?.let { adapter ->
                    when (adapter) {
                        is KuesionerAdapter -> {
                            adapter.updateData(result)
                        }
                    }
                }
            } else {
                makeToast("Kuesioner tidak ditemukan")
            }
        })
    }

    private fun launchKesimpulan(skor: Int) {
        viewModel.insertHasilKuesioner(
            HasilKuesionerPerkembangan(
                age,
                skor,
                formatDateNow()
            )
        )
        val intent = Intent(this, KesimpulanActivity::class.java)
        intent.putExtra(DATA_SKOR, skor)
        startActivity(intent)
        finish()
    }
}