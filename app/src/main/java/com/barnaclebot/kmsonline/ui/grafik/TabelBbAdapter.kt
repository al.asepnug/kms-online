package com.barnaclebot.kmsonline.ui.grafik

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.barnaclebot.kmsonline.data.entity.RecordBb
import com.barnaclebot.kmsonline.databinding.ItemBbBinding

class TabelBbAdapter (private val listItem: ArrayList<RecordBb>)
    : RecyclerView.Adapter<TabelBbAdapter.ViewHolder>(){

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemBbBinding.inflate(
            LayoutInflater.from(viewGroup.context),
            viewGroup,
            false
        )
        return ViewHolder(
            binding
        )
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listItem[position])
    }

    inner class ViewHolder(private val binding: ItemBbBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: RecordBb) {
            if (adapterPosition == itemCount-1) {
                binding.endVertical.visibility = View.VISIBLE
            }
            binding.tvBb.text = data.beratBadan.toString()
            binding.tvTglRecord.text = data.tanggalRecord
            binding.tvUmur.text = data.umur.toString()
        }
    }

    fun updateData(newList: List<RecordBb>) {
        listItem.clear()
        listItem.addAll(newList)
        notifyDataSetChanged()
    }
}