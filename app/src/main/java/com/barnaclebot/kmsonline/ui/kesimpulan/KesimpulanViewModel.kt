package com.barnaclebot.kmsonline.ui.kesimpulan

import androidx.lifecycle.ViewModel
import com.barnaclebot.kmsonline.data.DataRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class KesimpulanViewModel @Inject constructor(
    private val dataRepository: DataRepository
) : ViewModel() {

    fun getUser() = dataRepository.getUser()
}