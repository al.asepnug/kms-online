package com.barnaclebot.kmsonline.ui.profil

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import com.barnaclebot.kmsonline.R
import com.barnaclebot.kmsonline.data.entity.RecordBb
import com.barnaclebot.kmsonline.data.entity.RecordTb
import com.barnaclebot.kmsonline.data.entity.User
import com.barnaclebot.kmsonline.databinding.ActivityProfileBinding
import com.barnaclebot.kmsonline.ui.login.LoginActivity
import com.barnaclebot.kmsonline.utils.AgeCalculator.calculateAge
import com.barnaclebot.kmsonline.utils.AgeCalculator.calculateAgeInMonths
import com.barnaclebot.kmsonline.utils.AgeCalculator.parseDate
import com.barnaclebot.kmsonline.utils.ValueFormat
import com.barnaclebot.kmsonline.utils.ValueFormat.format2digit
import com.barnaclebot.kmsonline.utils.ValueFormat.formatDateNow
import com.barnaclebot.kmsonline.utils.ValueFormat.formatInUtcMilisecond
import com.barnaclebot.kmsonline.utils.makeToast
import com.google.android.material.datepicker.MaterialDatePicker
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException

@AndroidEntryPoint
class ProfileActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding: ActivityProfileBinding
    private val viewModel: ProfileViewModel by viewModels()
    private lateinit var dataUser: User
    private var editable: Boolean = false
    private var ageMonths: Int = 0

    companion object {
        const val DATA_USER = "data_user"
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.title = "Profil Bayi"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.btnEdit.setOnClickListener(this)
        binding.btnLogout.setOnClickListener(this)
        try {
            viewModel.getUser().observe(this, { result ->
                if (result != null) {
                    dataUser = result
                    ageMonths = calculateAgeInMonths(calculateAge(parseDate(dataUser.tanggalLahir)))
                    val datePicker = MaterialDatePicker.Builder.datePicker()
                        .setTitleText("Pilih Tanggal Lahir")
                        .setSelection(formatInUtcMilisecond(dataUser.tanggalLahir))
                        .build()
                    datePicker.addOnNegativeButtonClickListener { datePicker.dismiss() }
                    datePicker.addOnPositiveButtonClickListener {
                        binding.edtBirthDate.setText(ValueFormat.convertLongToTime(it))
                        datePicker.dismiss()
                    }

                    binding.edtBirthDate.setOnClickListener {
                        datePicker.show(supportFragmentManager, datePicker.toString())
                    }

                    binding.edtBirthDate.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
                        if (hasFocus) {
                            datePicker.show(supportFragmentManager, datePicker.toString())
                        } else {
                            datePicker.dismiss()
                        }
                    }
                    initUi()
                    viewModel.getRecordBbByUsia(ageMonths).observe(this, { result ->
                        if (result != null) {
                            binding.edtWeight.setText(result.beratBadan.toString())
                        } else {
                            binding.edtWeight.setText("masukan berat badan bulan ini")
                        }
                    })
                    viewModel.getRecordTbByUsia(ageMonths).observe(this, { result ->
                        if (result != null) {
                            binding.edtHeight.setText(result.tinggiBadan.toString())
                        } else {
                            binding.edtHeight.setText("masukan tinggi badan bulan ini")
                        }
                    })
                }
            })
        } catch (e: IOException) {
            makeToast("Error Load Data")
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnEdit -> {
                if (editable) {
                    if (binding.edtName.text.toString()
                            .isEmpty() || binding.edtBirthDate.text.toString().isEmpty() ||
                        binding.edtHeight.text.toString()
                            .isEmpty() || binding.edtWeight.text.toString()
                            .isEmpty() ||
                        binding.edtAddress.text.toString().isEmpty()
                    ) {
                        makeToast("Salah satu form tidak boleh kosong")
                    } else {
                        if (ValueFormat.validateValue(binding.edtHeight.text.toString()) && ValueFormat.validateValue(
                                binding.edtWeight.text.toString()
                            )
                        ) {
                            val user = User(
                                binding.edtName.text.toString(),
                                binding.edtBirthDate.text.toString(),
                                if (R.id.rbLaki == binding.rbGroup.checkedRadioButtonId) 0 else 1,
                                binding.edtAddress.text.toString()
                            )
                            try {
                                viewModel.updateUser(dataUser.id, user)
                                viewModel.insertRecordBb(
                                    RecordBb(
                                        calculateAgeInMonths(calculateAge(parseDate(binding.edtBirthDate.text.toString()))),
                                        format2digit(binding.edtWeight.text.toString()),
                                        formatDateNow()
                                    )
                                )
                                viewModel.insertRecordTb(
                                    RecordTb(
                                        calculateAgeInMonths(calculateAge(parseDate(binding.edtBirthDate.text.toString()))),
                                        format2digit(binding.edtHeight.text.toString()),
                                        formatDateNow()
                                    )
                                )
                                makeToast("Sukses Merubah Data Bayi")
                                initUi()
                            } catch (exc: Exception) {
                                makeToast("Gagal Merubah Data Bayi")
                            }
                        } else {
                            makeToast("Format nilai berat / tinggi badan masih salah, pastikan tidak lebih 4 digit dan nilai maksimal 100")
                        }
                    }
                } else {
                    enableEdited()
                }
            }

            R.id.btnLogout -> {
                val intent = Intent(this, LoginActivity::class.java)
                intent.addFlags(
                    Intent.FLAG_ACTIVITY_CLEAR_TOP or
                            Intent.FLAG_ACTIVITY_CLEAR_TASK or
                            Intent.FLAG_ACTIVITY_NEW_TASK
                )
                startActivity(intent)
            }
        }
    }

    private fun initUi() {
        editable = false
        binding.btnEdit.text = "Ubah"
        binding.edtName.isEnabled = false
        binding.edtBirthDate.isEnabled = false
        binding.edtWeight.isEnabled = false
        binding.edtHeight.isEnabled = false
        binding.edtAddress.isEnabled = false
        binding.edtName.setText(dataUser.nama)
        binding.edtBirthDate.setText(dataUser.tanggalLahir)
        binding.edtAddress.setText(dataUser.alamat)
        binding.rbLaki.isClickable = false
        binding.rbPerempuan.isClickable = false
        if (dataUser.jenisKelamin == 0) {
            binding.rbLaki.isChecked = true
        } else {
            binding.rbPerempuan.isChecked = true
        }
    }

    private fun enableEdited() {
        binding.btnEdit.text = "Simpan"
        binding.edtName.isEnabled = true
        binding.edtBirthDate.isEnabled = true
        binding.edtWeight.isEnabled = true
        binding.edtHeight.isEnabled = true
        binding.edtAddress.isEnabled = true
        binding.rbLaki.isClickable = true
        binding.rbPerempuan.isClickable = true
        if (dataUser.jenisKelamin == 0) {
            binding.rbLaki.isChecked = true
        } else {
            binding.rbPerempuan.isChecked = true
        }
        editable = true
    }
}