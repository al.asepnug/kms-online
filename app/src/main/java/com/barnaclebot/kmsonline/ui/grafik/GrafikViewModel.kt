package com.barnaclebot.kmsonline.ui.grafik

import androidx.lifecycle.ViewModel
import com.barnaclebot.kmsonline.data.DataRepository
import com.barnaclebot.kmsonline.data.entity.*
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class GrafikViewModel @Inject constructor(
    private val dataRepository: DataRepository
) : ViewModel(){
    fun getUser() = dataRepository.getUser()

    fun updateUser(idUser: Int, user: User) = dataRepository.updateUser(idUser, user)

    fun getRecordBb() = dataRepository.getRecordBb()

    fun insertRecordBb(data: RecordBb) = dataRepository.insertRecordBb(data)

    fun getRecordTb() = dataRepository.getRecordTb()

    fun insertRecordTb(data: RecordTb) = dataRepository.insertRecordTb(data)

    fun getBeratBadan(gender: Int) = dataRepository.getBeratBadan(gender)

    fun getTinggiBadan(gender: Int) = dataRepository.getTinggiBadan(gender)
}