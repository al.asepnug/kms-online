package com.barnaclebot.kmsonline.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.OnFocusChangeListener
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.barnaclebot.kmsonline.R
import com.barnaclebot.kmsonline.data.entity.RecordBb
import com.barnaclebot.kmsonline.data.entity.RecordTb
import com.barnaclebot.kmsonline.data.entity.User
import com.barnaclebot.kmsonline.databinding.ActivityLoginBinding
import com.barnaclebot.kmsonline.ui.kuesioner.KuesionerActivity
import com.barnaclebot.kmsonline.utils.AgeCalculator.calculateAge
import com.barnaclebot.kmsonline.utils.AgeCalculator.calculateAgeInMonths
import com.barnaclebot.kmsonline.utils.AgeCalculator.parseDate
import com.barnaclebot.kmsonline.utils.ValueFormat.convertLongToTime
import com.barnaclebot.kmsonline.utils.ValueFormat.format2digit
import com.barnaclebot.kmsonline.utils.ValueFormat.formatDateNow
import com.barnaclebot.kmsonline.utils.ValueFormat.validateValue
import com.barnaclebot.kmsonline.utils.makeToast
import com.google.android.material.datepicker.MaterialDatePicker
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding: ActivityLoginBinding
    private val viewModel: LoginViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.btnNext.setOnClickListener(this)
        val datePicker = MaterialDatePicker.Builder.datePicker()
            .setTitleText("Pilih Tanggal Lahir")
            .setSelection(MaterialDatePicker.todayInUtcMilliseconds())
            .build()
        datePicker.addOnNegativeButtonClickListener { datePicker.dismiss() }
        datePicker.addOnPositiveButtonClickListener {
            binding.edtBirthDate.setText(convertLongToTime(it))
            datePicker.dismiss()
        }

        binding.edtBirthDate.setOnClickListener {
            datePicker.show(supportFragmentManager, datePicker.toString())
        }

        binding.edtBirthDate.onFocusChangeListener = OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                datePicker.show(supportFragmentManager, datePicker.toString())
            } else {
                datePicker.dismiss()
            }
        }

    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnNext -> {
                if (binding.edtName.text.toString()
                        .isEmpty() || binding.edtBirthDate.text.toString().isEmpty() ||
                    binding.edtHeight.text.toString().isEmpty() || binding.edtWeight.text.toString()
                        .isEmpty() ||
                    binding.edtAddress.text.toString().isEmpty()
                ) {
                    makeToast("Salah satu form tidak boleh kosong")
                } else {
                    if (validateValue(binding.edtHeight.text.toString()) && validateValue(binding.edtWeight.text.toString())) {
                        val dataUser = User(
                            binding.edtName.text.toString(),
                            binding.edtBirthDate.text.toString(),
                            if (R.id.rbLaki == binding.rbGroup.checkedRadioButtonId) 0 else 1,
                            binding.edtAddress.text.toString()
                        )
                        try {
                            viewModel.insertUser(dataUser)
                            val age = calculateAgeInMonths(calculateAge(parseDate(dataUser.tanggalLahir)))
                            viewModel.insertRecordBb(RecordBb(age, format2digit(binding.edtWeight.text.toString()),
                                formatDateNow()))
                            viewModel.insertRecordTb(RecordTb(age, format2digit(binding.edtHeight.text.toString()),
                                formatDateNow()))
                            viewModel.setIsLogin(true)
                            makeToast("Sukses Memasukkan Data Bayi")
                            val intent = Intent(this, KuesionerActivity::class.java)
                            intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP or
                                    Intent.FLAG_ACTIVITY_CLEAR_TASK or
                                    Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(intent)
                        } catch (exc: Exception) {
                            makeToast("Gagal Memasukkan Data Bayi")
                        }
                    } else {
                        makeToast("Format nilai berat / tinggi badan masih salah, pastikan tidak lebih 4 digit dan nilai maksimal 100")
                    }
                }
            }
        }
    }
}