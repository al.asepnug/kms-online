package com.barnaclebot.kmsonline.ui.kuesioner

import androidx.lifecycle.ViewModel
import com.barnaclebot.kmsonline.data.DataRepository
import com.barnaclebot.kmsonline.data.entity.HasilKuesionerPerkembangan
import com.barnaclebot.kmsonline.data.entity.KuesionerPerkembangan
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class KuesionerViewModel @Inject constructor(
    private val dataRepository: DataRepository
) : ViewModel() {

    fun getKuesioner(age: Int) = dataRepository.getKuesioner(age)

    fun getHasilKuesionerByUsia(age: Int) = dataRepository.getHasilKuesionerByUsia(age)

    fun getUser() = dataRepository.getUser()

    fun insertHasilKuesioner(data: HasilKuesionerPerkembangan) = dataRepository.insertHasilKuesioner(data)

    fun getKuesionerVersioning(): Int = dataRepository.getKuesionerVersioning()

    fun insertKuesionerVersioning(dataVersioning: Int) = dataRepository.setKuesionerVersioning(dataVersioning)

    fun insertKuesioner(dataKuesionerPerkembangan: List<KuesionerPerkembangan>) = dataRepository.insertKuesioner(dataKuesionerPerkembangan)
}