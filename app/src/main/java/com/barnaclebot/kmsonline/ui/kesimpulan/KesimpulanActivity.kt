package com.barnaclebot.kmsonline.ui.kesimpulan

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import com.barnaclebot.kmsonline.R
import com.barnaclebot.kmsonline.databinding.ActivityKesimpulanBinding
import com.barnaclebot.kmsonline.ui.main.MainActivity
import com.barnaclebot.kmsonline.utils.makeToast
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException

@AndroidEntryPoint
class KesimpulanActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding: ActivityKesimpulanBinding
    private val viewModel: KesimpulanViewModel by viewModels()

    companion object {
        const val DATA_SKOR = "data_skor"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityKesimpulanBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.btnDone.setOnClickListener(this)
        try {
            populateUser()
            initUi(intent.extras?.getInt(DATA_SKOR)!!)
        } catch (e: IOException) {
            makeToast("Error Load Data")
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnDone -> {
                val intent = Intent(this, MainActivity::class.java)
                intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP or
                        Intent.FLAG_ACTIVITY_CLEAR_TASK or
                        Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                finish()
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun initUi(skor: Int) {
        when (skor) {
            in 9..20 -> {
                binding.tvIntervensi.text = "Perkembangan Bayi Normal"
                binding.tvKeterangan.text = getString(R.string.kesimpulan_normal)
            }
            in 7..8 -> {
                binding.tvIntervensi.text = "Perkembangan Bayi Meragukan"
                binding.tvKeterangan.text = getString(R.string.kesimpulan_meragukan)
            }
            else -> {
                binding.tvIntervensi.text = "Perkembangan Bayi Menyimpang"
                binding.tvKeterangan.text = getString(R.string.kesimpulan_menyimpang)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun populateUser() {
        viewModel.getUser().observe(this, { result ->
            if (result != null) {
                binding.tvNamaBayi.text = "Hi, ${result.nama}"
            } else {
                makeToast("Data User Kosong")
            }
        })
    }


}